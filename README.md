<div align="center">
   <a href="https://gitlab.com/Asperatus/osb"><img src="https://asperatus.gitlab.io/assets/osb/rounded.png" style="width: 41px; border-radius: 20px;" /></a>
   <p></p> a <i>fast</i>, <i>fully-fledged</i>, <i>scalable</i> and <i>secure</i> implementation of the <b>.osb</b> storyboard file format in Rust
</div>
<br/>
<div align="center">
  <a href="https://pochiii.com"><img src="https://img.shields.io/badge/curated%20by-pochiii-000?style=for-the-badge" /></a>
  <a href="https://crates.io/crates/osb"><img src="https://img.shields.io/crates/v/osb?style=for-the-badge"/></a>
  <a href="https://crates.io/crates/osb"><img src="https://img.shields.io/crates/d/osb?style=for-the-badge"/></a>
  <a href="https://gitlab.com/Asperatus/osb/-/pipelines/latest"><img src="https://img.shields.io/gitlab/pipeline/Asperatus/osb/master?style=for-the-badge"/></a>
  <a href="https://gitlab.com/Asperatus/osb"><img src="https://img.shields.io/gitlab/coverage/Asperatus/osb/master?style=for-the-badge"/></a>
  <a href="https://docs.rs/osb"><img src="https://img.shields.io/badge/Read%20the%20-docs-informational?style=for-the-badge"/></a>
  <a href="#license"><img src="https://img.shields.io/crates/l/osb?style=for-the-badge"/></a>
  <a href="https://gitter.im/osb-rs/community"><img src="https://img.shields.io/badge/chat-on%20gitter-4db798?style=for-the-badge"/></a>
</div>
<br/>

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
